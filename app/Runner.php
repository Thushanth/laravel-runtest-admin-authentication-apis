<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Runner extends Model
{
    protected $table = 'runners';
    protected $fillable = ['name', 'race_id', 'external_id'];

    public function runner()
    {
        return $this->belongsTo('App\Race', 'race_id');
    }
    public function formData()
    {
        return $this->hasMany('App\Runner', 'runner_id');
    }
}
