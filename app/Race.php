<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $table = 'races';
    protected $fillable = ['name', 'meeting_id', 'external_id'];

    public function race()
    {
        return $this->belongsTo('App\Meeting', 'meeting_id');
    }
    public function runnerName()
    {
        return $this->hasMany('App\Runner', 'race_id');
    }
}
