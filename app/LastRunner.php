<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastRunner extends Model
{
    protected $table = 'last_runs';
    protected $fillable = ['runner_id', 'date', 'score','award','status'];

    public function lastrundata()
    {
        return $this->belongsTo('App\Runner', 'runner_id');
    }
}
