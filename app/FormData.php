<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormData extends Model
{
    protected $table = 'forms_data';
    protected $fillable = ['runner_id', 'age', 'gender','color','weight','place'];

    public function formdata()
    {
        return $this->belongsTo('App\Runner', 'runner_id');
    }
}
