<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Runner;
use App\FormData;

class FormDataController extends Controller
{
    public function __construct(Runner $runner, FormData $formData)
    {       
        $this->data = $formData;
        $this->runner = $runner;
        $this->middleware('auth');
    }

    public function index($id = null)
    {
        
        $allData = $this->data->orderBy('id', 'DESC')->get();
        $runnerData = $this->runner->pluck('name', 'id'); 

        if($id) {
            $singleData = $this->data->find($id);
        }else {
            $singleData = new FormData();
        }

        return view('admin.runner.formData', compact('allData', 'singleData','runnerData'));
    }

    public function post_add(Request $request)
    {        

        $this->data->fill($request->all());        
        
        $this->data->save();
        
        return redirect('admin/formdata')->with('success', 'Data has been created');
    }

    public function post_edit(Request $request, $id)
    {
        $this->data = $this->data->find($id);

        $this->data->fill($request->all());

        $this->data->save();

        return redirect('admin/formdata')->with('success', 'Data has been updated');
    }

    public function get_delete($id)
    {
        $this->data->find($id)->delete();
        
        return redirect('admin/formdata')->with('success', 'Your data has been deleted successfully.');       
            
    }
}
