<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Meeting;
use App\Race;
use App\Runner;

class RaceController extends Controller
{
    public function __construct(Meeting $meeting, Race $race, Runner $runner)
    {       
        $this->data = $race;
        $this->meeting = $meeting;
        $this->runner = $runner;
        $this->middleware('auth');
    }

    public function index($id = null)
    {
        
        $allData = $this->data->orderBy('id', 'DESC')->get();
        $meetingData = $this->meeting->pluck('name', 'id'); 

        if($id) {
            $singleData = $this->data->find($id);
        }else {
            $singleData = new Race();
        }

        return view('admin.race.race', compact('allData', 'singleData','meetingData'));
    }

    public function post_add(Request $request)
    {        

        $this->data->fill($request->all());        
        
        $this->data->save();
        $sessionMsg = $this->data->name;
        return redirect('admin/race')->with('success', 'Data '.$sessionMsg.' has been created');
    }

    public function post_edit(Request $request, $id)
    {
        $this->data = $this->data->find($id);

        $this->data->fill($request->all());

        $this->data->save();

        $sessionMsg = $this->data->name;
        return redirect('admin/race')->with('success', 'Data '.$sessionMsg.' has been updated');
    }

    public function get_delete($id)
    {
        $runData = $this->runner->where('race_id', $id)->get();

        if(count($runData)>0) {
            return redirect('admin/race')->with('error', 'Please delete corresponding data');
        }else {
            $this->data->find($id)->delete();
            return redirect('admin/race')->with('success', 'Your data has been deleted successfully.');       
        }
        
    }
}
