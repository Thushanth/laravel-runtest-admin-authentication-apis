<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormData;
use App\LastRunner;
use App\Race;
use App\Runner;

class RunnerController extends Controller
{
    public function __construct(Race $race, Runner $runner, FormData $formData, LastRunner $lastRunner)
    {       
        $this->data = $runner;
        $this->race = $race;
        $this->formData = $formData;
        $this->lastRunner = $lastRunner;
        $this->middleware('auth');
    }

    public function index($id = null)
    {
        
        $allData = $this->data->orderBy('id', 'DESC')->get();
        $raceData = $this->race->pluck('name', 'id'); 

        if($id) {
            $singleData = $this->data->find($id);
        }else {
            $singleData = new Runner();
        }

        return view('admin.runner.runner', compact('allData', 'singleData','raceData'));
    }

    public function post_add(Request $request)
    {        

        $this->data->fill($request->all());        
        
        $this->data->save();
        $sessionMsg = $this->data->name;
        return redirect('admin/runner')->with('success', 'Data '.$sessionMsg.' has been created');
    }

    public function post_edit(Request $request, $id)
    {
        $this->data = $this->data->find($id);

        $this->data->fill($request->all());

        $this->data->save();

        $sessionMsg = $this->data->name;
        return redirect('admin/runner')->with('success', 'Data '.$sessionMsg.' has been updated');
    }

    public function get_delete($id)
    {
        $formData = $this->formData->where('runner_id', $id)->get();
        $lastRunner = $this->lastRunner->where('runner_id', $id)->get();

        if(count($lastRunner && $formData  )>0) {
            return redirect('admin/runner')->with('error', 'Please delete corresponding data');
        }else {
            $this->data->find($id)->delete();
            return redirect('admin/runner')->with('success', 'Your data has been deleted successfully.');       
        }
        
    }
}
