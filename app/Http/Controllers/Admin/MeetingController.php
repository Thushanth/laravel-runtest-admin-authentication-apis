<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Meeting;
use App\Race;

class MeetingController extends Controller
{
    public function __construct(Meeting $meeting, Race $race)
    {       
        $this->data = $meeting;
        $this->race = $race;
        $this->middleware('auth');
    }

    public function index($id = null)
    {
        
        $allData = $this->data->orderBy('id', 'DESC')->get();

        if($id) {
            $singleData = $this->data->find($id);
        }else {
            $singleData = new Meeting();
        }

        return view('admin.meeting.meeting', compact('allData', 'singleData'));
    }

    public function post_add(Request $request)
    {        

        $this->data->fill($request->all());        
        
        $this->data->save();
        $sessionMsg = $this->data->name;
        return redirect('admin/meeting')->with('success', 'Data '.$sessionMsg.' has been created');
    }

    public function post_edit(Request $request, $id)
    {
        $this->data = $this->data->find($id);

        $this->data->fill($request->all());

        $this->data->save();

        $sessionMsg = $this->data->name;
        return redirect('admin/meeting')->with('success', 'Data '.$sessionMsg.' has been updated');
    }

    public function get_delete($id)
    {
        $raceData = $this->race->where('meeting_id', $id)->get();

        if(count($raceData)>0) {
            return redirect('admin/meeting')->with('error', 'Please delete corresponding data before delete the meeting');
        }else {
            $this->data->find($id)->delete();
            return redirect('admin/meeting')->with('success', 'Your data has been deleted successfully.');       
        }
        
    }
}
