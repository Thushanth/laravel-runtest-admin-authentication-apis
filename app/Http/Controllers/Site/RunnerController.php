<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Meeting;
use App\Race;
use App\Runner;
use App\FormData;
use App\LastRunner;

class RunnerController extends Controller
{
    public function __construct(Runner $runner, FormData $formdata, Race $racedata, LastRunner $lastrun)
    {       
        $this->data = $runner;
        $this->formdata = $formdata;
        $this->racedata = $racedata;
        $this->lastrun = $lastrun;
    }

    public function get_all()
    {   

       $allData = $this->data->orderBy('id', 'DESC')->paginate(20);
       $raceData = $this->racedata->orderBy('id', 'DESC')->paginate(20);     

        return view('site.home.index', compact('allData','raceData'));
    }

    public function get_single($id)
    {
        $singleData = $this->data->find($id);  
        $singleformData = $this->formdata->where('runner_id', $singleData->id)->first();  
        $singlelastData = $this->lastrun->where('runner_id', $singleData->id)->first();  
        // dd($singleformData)  ; 
        return view('site.home.single',compact('singleData','singleformData','singlelastData'));
    }
}
