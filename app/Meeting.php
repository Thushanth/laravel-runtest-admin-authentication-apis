<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $table = 'meetings';
    protected $fillable = ['name','external_id'];
    
    public function meetingName()
    {
        return $this->hasMany('App\Race', 'meeting_id');
    }
}
