<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('site.home.home');
// });
Route::get('/', 'Site\RunnerController@get_all');
Route::get('/runner/{id}/formdata', 'Site\RunnerController@get_single');

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' =>  ['auth']],function (){

    Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');

    //meeting routes
    Route::get('/meeting', 'Admin\MeetingController@index');
    Route::post('/meeting', 'Admin\MeetingController@post_add');
    Route::get('/meeting/{id}/edit', 'Admin\MeetingController@index');
    Route::post('/meeting/{id}/edit', 'Admin\MeetingController@post_edit');
    Route::get('/meeting/{id}/delete', 'Admin\MeetingController@get_delete');

    //race routes
    Route::get('/race', 'Admin\RaceController@index');
    Route::post('/race', 'Admin\RaceController@post_add');
    Route::get('/race/{id}/edit', 'Admin\RaceController@index');
    Route::post('/race/{id}/edit', 'Admin\RaceController@post_edit');
    Route::get('/race/{id}/delete', 'Admin\RaceController@get_delete');

     //runner routes
     Route::get('/runner', 'Admin\RunnerController@index');
     Route::post('/runner', 'Admin\RunnerController@post_add');
     Route::get('/runner/{id}/edit', 'Admin\RunnerController@index');
     Route::post('/runner/{id}/edit', 'Admin\RunnerController@post_edit');
     Route::get('/runner/{id}/delete', 'Admin\RunnerController@get_delete');

     //formData routes
     Route::get('/formdata', 'Admin\FormDataController@index');
     Route::post('/formdata', 'Admin\FormDataController@post_add');
     Route::get('/formdata/{id}/edit', 'Admin\FormDataController@index');
     Route::post('/formdata/{id}/edit', 'Admin\FormDataController@post_edit');
     Route::get('/formdata/{id}/delete', 'Admin\FormDataController@get_delete');

     //LastrunData routes
     Route::get('/lastruns', 'Admin\LastRunnerController@index');
     Route::post('/lastruns', 'Admin\LastRunnerController@post_add');
     Route::get('/lastruns/{id}/edit', 'Admin\LastRunnerController@index');
     Route::post('/lastruns/{id}/edit', 'Admin\LastRunnerController@post_edit');
     Route::get('/lastruns/{id}/delete', 'Admin\LastRunnerController@get_delete');


});
