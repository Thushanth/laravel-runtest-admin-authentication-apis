
## Create project using composer

step 1 :- composer global require laravel/installer
step 2 :- laravel new runtest.dev
Make sure you have the php version 7.3 or greater.

Laravel version 6.0.

## Make Authentication

step 1 :- composer require laravel/ui "^1.0" --dev

step 2 :- php artisan ui vue --auth

## create Database

step 1 :- create a database in mysql

step 2 :- open your project -> .env 

step 3 :- change username, DB name, other credintials

step 4 :- open you terminal and enter "php artisan migrate"

## File Create 

To create your controller :- php artisan make:controller <Controller_name>

To create your models :- php artisan make:model <model_name> 

To create your database :- php artisan make:migration <create_users_table>

TO migrate the tables :- php artisan migrate

## SET up the view files

Set up your view files according to you requirement

## External libraries

Here, we use HTML libraries for forms

composer require laravelcollective/html

## Host

To localhost view of your project type the following command in terminal:-

php artisan serve



