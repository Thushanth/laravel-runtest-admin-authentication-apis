@extends('layouts.app')
@section('content')
    <div class="breadcrumb-section jarallax pixels-bg" data-jarallax data-speed="0.6">
        <div class="container text-center">
            <h1>{{ ($singleData->name) }}</h1>
            
        </div>
    </div>


    <div class="section-block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                    <div class="blog-list inno_shadow p-3">  
                        @if($singleformData)
                        <div class="table-responsive">
                            <table class="table table-hover">
                            <thead><strong> <h3>Basic Information </h3></strong></thead>
                            <tr>
                              <th>Profile Name:</th>
                              <td>{{ ($singleData->name) }}</td>
                            </tr>
                            <tr>
                              <th>Age:</th>
                              <td>{{$singleformData->age}}</td>
                            </tr>
                            <tr>
                              <th>Gender:</th>
                              <td>{{$singleformData->gender}}</td>
                            </tr>
                            <tr>
                              <th>Skin Color:</th>
                              <td>{{$singleformData->color}}</td>
                              </tr>
                            <tr>
                                <th>Weight:</th>
                                <td>{{$singleformData->weight}} Kg</td>
                            </tr>
                            <tr>
                                <th>Living Place:</th>
                                <td>{{$singleformData->place}}</td>
                            </tr>
                          </table> 
                        </div> 
                        @endif     
                        
                        @if($singlelastData)
                        <div class="table-responsive">
                            <table class="table table-hover">
                            <thead><strong><h3>Runner Lastruns Information</h3></strong></thead>
                            <tr>
                              <th>Event Date:</th>
                              <td>{{ ($singlelastData->date) }}</td>
                            </tr>
                            <tr>
                              <th>Score:</th>
                              <td>{{$singlelastData->score}}</td>
                            </tr>
                            <tr>
                              <th>Event Status:</th>
                              <td>{{$singlelastData->status}}</td>
                            </tr>
                            <tr>
                              <th>Award:</th>
                              <td>{{$singlelastData->award}}</td>
                            </tr>
                          </table> 
                        </div> 
                        @endif               
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection