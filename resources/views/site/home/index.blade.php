<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Runner Page</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            

            <div class="content">
                <div class="container">
                    <div class="col-md-10 col-sm-12 col-12 offset-md-1">
                        <div class="blog-list inno_shadow p-3"> 
                            @if($raceData)
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Meeting Name</th>
                                        <th>Race Name</th>
                                      </tr>
                                @foreach($raceData as $raceData)
                                <tr>
                                  <td>{{$raceData->race->name}}</td>
                                  <td>{{$raceData->name}}</td>
                                </tr>
                                @endforeach
                              </table>
                            </div>
                            @endif
                        
                        </div>
                    </div>
                </div>
                <div class="section-block">
                    <div class="container">
                        <div class="row">
                            @foreach($allData as $row)
                                <div class="col-md-4 col-sm-6 col-12" style="border-radius:10px; margin:10px;">
                                    <div class="blog-grid shadow_a p-3d">
                                        <div class="blog-team-box">
                                            <h6>Created at: {{ date('d M, Y', strtotime($row->created_at)) }}</h6>
                                        </div>
                                        <div class="p-3">
                                            <h4>
                                                <a href="{{ url('runner/'.$row->id. '/formdata')}}">{{ ($row->name) }}</a>
                                                <div class="blog-team-box">
                                                    <div>ExternalData:- {{$row->external_id}}</div>
                                                    <div>Race:- {{$row->runner->name}}</div>
                                                </div>
                                            </h4>
                                           
                                            <a href="{{ url('runner/'.$row->id. '/formdata')}}" class="button-simple-primary mt-20">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
