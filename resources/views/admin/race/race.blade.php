@extends('layouts.app')

@section('content')
<div class="container">
<div class="nav-tabs-custom">    
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="row">
                <div class="col-md-6">
                    {!! Form::model($singleData, array('files' => true, 'autocomplete' => 'off')) !!}
                    {!!csrf_field()!!}
                    <?php $externalData = ['A'=>'A', 'B'=>'B', 'C'=>'C'];?>
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label("Race Name") !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Race name','required']) !!}
                            <em class="error-msg">{!!$errors->first('name')!!}</em>
                        </div>                        
                        
                        <div class="form-group {{ $errors->has('external_id') ? 'has-error' : '' }}">
                            {!! Form::label("External Data") !!}
                            {!! Form::select('external_id', $externalData, null, ['class' => 'form-control', 'placeholder' => 'Select External Data','required']) !!}
                            <em class="error-msg">{!!$errors->first('external_id')!!}</em>
                        </div> 

                        <div class="form-group {{ $errors->has('meeting_id') ? 'has-error' : '' }}">
                            {!! Form::label("Meeting") !!}
                            {!! Form::select('meeting_id', $meetingData, null, ['class' => 'form-control', 'placeholder' => 'Select Meeting','required']) !!}
                            <em class="error-msg">{!!$errors->first('meeting_id')!!}</em>
                        </div>   
                    </div>
                    <div class="box-footer">                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> @if($singleData->id) Update @else Create @endif
                            </button>
                            <button type="reset" class="btn btn-default">
                                <i class="fas fa-sync-alt"></i> Reset
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="col-md-6">
                    <div class="box-header">
                        <h3 class="box-title">List of Races</h3>                       
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Race Name</th>                                       
                                        <th>External </th>
                                        <th>Meeting Name </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 0; ?>
                                    @foreach ($allData as $row)
                                    <?php $count++; ?>
                                    <tr class="">
                                        <td>{{$count}}</td>
                                        <td>{!!$row->name!!}</td>   
                                        <td>{!!$row->external_id!!}</td>   
                                        <td>{!!$row->race->name!!}</td>                                     
                                        <td>
                                            <a href="{{ URL::to('admin/race/'.$row->id.'/edit')}}" class="btn btn-sm btn-warning"> Edit</a>
                                            <a href="{{ URL::to('admin/race/'.$row->id.'/delete')}}" onclick="if(!confirm('Are you sure to delete this data?')){return false;}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection