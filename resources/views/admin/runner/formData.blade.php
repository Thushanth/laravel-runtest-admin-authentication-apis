@extends('layouts.app')

@section('content')
<div class="container">
<div class="nav-tabs-custom">    
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::model($singleData, array('files' => true, 'autocomplete' => 'off')) !!}
                    {!!csrf_field()!!}
                    <?php $genderData = ['Male'=>'Male', 'Female'=>'Female', 'None of above'=>'None of above'];?>
                    <?php $colorData = ['White'=>'White', 'Black'=>'Black', 'None of above'=>'None of above'];?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('runner_id') ? 'has-error' : '' }}">
                                    {!! Form::label("Runner Name") !!}
                                    {!! Form::select('runner_id',$runnerData, null, ['class' => 'form-control', 'placeholder' => 'Select Runner','required']) !!}
                                    <em class="error-msg">{!!$errors->first('runner_id')!!}</em>
                                </div>                        
                                
                                <div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
                                    {!! Form::label("Age") !!}
                                    {!! Form::text('age', null, ['class' => 'form-control', 'placeholder' => 'Enter Age','required']) !!}
                                    <em class="error-msg">{!!$errors->first('age')!!}</em>
                                </div> 

                                <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                                    {!! Form::label("Gender") !!}
                                    {!! Form::select('gender', $genderData, null, ['class' => 'form-control', 'placeholder' => 'Select Gender','required']) !!}
                                    <em class="error-msg">{!!$errors->first('gender')!!}</em>
                                </div>
                            </div> 
                        </div>

                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('color') ? 'has-error' : '' }}">
                                    {!! Form::label("Color") !!}
                                    {!! Form::select('color',$colorData, null, ['class' => 'form-control', 'placeholder' => 'Select Color','required']) !!}
                                    <em class="error-msg">{!!$errors->first('color')!!}</em>
                                </div>                        
                                
                                <div class="form-group {{ $errors->has('weight') ? 'has-error' : '' }}">
                                    {!! Form::label("Weight") !!}
                                    {!! Form::text('weight', null, ['class' => 'form-control', 'placeholder' => 'Enter Weight(Kg)','required']) !!}
                                    <em class="error-msg">{!!$errors->first('weight')!!}</em>
                                </div> 

                                <div class="form-group {{ $errors->has('place') ? 'has-error' : '' }}">
                                    {!! Form::label("Place") !!}
                                    {!! Form::text('place', null, ['class' => 'form-control', 'placeholder' => 'Enter Place','required']) !!}
                                    <em class="error-msg">{!!$errors->first('place')!!}</em>
                                </div>
                            </div> 
                        </div>    
                    </div>
                    <div class="box-footer">                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> @if($singleData->id) Update @else Create @endif
                            </button>
                            <button type="reset" class="btn btn-default">
                                <i class="fas fa-sync-alt"></i> Reset
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="col-12">
                    <div class="box-header">
                        <h3 class="box-title">List of FormData</h3>                       
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Runner Name</th>                                       
                                        <th>Age</th>
                                        <th>Gender</th>
                                        <th>Color</th>
                                        <th>Weight</th>
                                        <th>Place</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 0; ?>
                                    @foreach ($allData as $row)
                                    <?php $count++; ?>
                                    <tr class="">
                                        <td>{{$count}}</td>
                                        <td>{!!$row->formdata->name!!}</td>   
                                        <td>{!!$row->age!!}</td>   
                                        <td>{!!$row->gender!!}</td> 
                                        <td>{!!$row->color!!}</td>   
                                        <td>{!!$row->weight!!} Kg</td>   
                                        <td>{!!$row->place!!}</td>                                      
                                        <td>
                                            <a href="{{ URL::to('admin/formdata/'.$row->id.'/edit')}}" class="btn btn-sm btn-warning"> Edit</a>
                                            <a href="{{ URL::to('admin/formdata/'.$row->id.'/delete')}}" onclick="if(!confirm('Are you sure to delete this data?')){return false;}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection