@extends('layouts.app')

@section('content')
<div class="container">
<div class="nav-tabs-custom">    
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::model($singleData, array('files' => true, 'autocomplete' => 'off')) !!}
                    {!!csrf_field()!!}
                    <?php $genderData = ['Male'=>'Male', 'Female'=>'Female', 'None of above'=>'None of above'];?>
                    <?php $colorData = ['White'=>'White', 'Black'=>'Black', 'None of above'=>'None of above'];?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('runner_id') ? 'has-error' : '' }}">
                                    {!! Form::label("Runner Name") !!}
                                    {!! Form::select('runner_id',$lastrunnerData, null, ['class' => 'form-control', 'placeholder' => 'Select Runner','required']) !!}
                                    <em class="error-msg">{!!$errors->first('runner_id')!!}</em>
                                </div>                        
                                
                                <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                                    {!! Form::label("Date") !!}
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calender"></i>
                                        </div>
                                        {!! Form::text('date', null, ['id' => 'datepicker','class' => 'form-control', 'placeholder' => 'Select Last Match Date','required']) !!}
                                    </div>
                                    <em class="error-msg">{!!$errors->first('date')!!}</em>
                                </div> 

                                <div class="form-group {{ $errors->has('score') ? 'has-error' : '' }}">
                                    {!! Form::label("Score") !!}
                                    {!! Form::text('score', null, ['class' => 'form-control', 'placeholder' => 'Enter Score','required']) !!}
                                    <em class="error-msg">{!!$errors->first('score')!!}</em>
                                </div>
                            </div> 
                        </div>

                        <div class="col-md-6">
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('award') ? 'has-error' : '' }}">
                                    {!! Form::label("Award") !!}
                                    {!! Form::text('award', null, ['class' => 'form-control', 'placeholder' => 'Enter Award Type','required']) !!}
                                    <em class="error-msg">{!!$errors->first('award')!!}</em>
                                </div>                        
                                
                                <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                    {!! Form::label("Runner Status") !!}
                                    {!! Form::text('status', null, ['class' => 'form-control', 'placeholder' => 'Enter Runners Status','required']) !!}
                                    <em class="error-msg">{!!$errors->first('status')!!}</em>
                                </div> 
                            </div> 
                        </div>    
                    </div>
                    <div class="box-footer">                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-check-circle"></i> @if($singleData->id) Update @else Create @endif
                            </button>
                            <button type="reset" class="btn btn-default">
                                <i class="fas fa-sync-alt"></i> Reset
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="col-12">
                    <div class="box-header">
                        <h3 class="box-title">List of LastRuns Data</h3>                       
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Runner Name</th>                                       
                                        <th>LastMatch Date</th>
                                        <th>Score</th>
                                        <th>Award</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $count = 0; ?>
                                    @foreach ($allData as $row)
                                    <?php $count++; ?>
                                    <tr class="">
                                        <td>{{$count}}</td>
                                        <td>{!!$row->lastrundata->name!!}</td>   
                                        <td>{!!$row->date!!}</td>   
                                        <td>{!!$row->score!!}</td> 
                                        <td>{!!$row->award!!}</td>   
                                        <td>{!!$row->status!!}</td>                                         
                                        <td>
                                            <a href="{{ URL::to('admin/lastruns/'.$row->id.'/edit')}}" class="btn btn-sm btn-warning"> Edit</a>
                                            <a href="{{ URL::to('admin/lastruns/'.$row->id.'/delete')}}" onclick="if(!confirm('Are you sure to delete this data?')){return false;}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
    
    <!-- <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        });   
    </script> -->

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#datepicker").datetimepicker({
                format: 'YYYY-MM-DD ',
                autoclose: true,
                todayHighlight: true
            });
       });
    </script>
@endsection
